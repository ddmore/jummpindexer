/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import net.biomodels.jummp.model.ModelFormat
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertTrue

/**
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 */
@RunWith(JUnit4.class)
class DDMoReIndexingIT {
    private static final Logger logger = LoggerFactory.getLogger(DDMoReIndexingIT.class)

    @Test
    void testIndexingPharmML() {
        File jsonFile = new File("src/test/resources/models/pharmml-rdf/indexData.json")
        def pharmML_061 = new ModelFormat(name: "PharmML", identifier: "PharmML",
                formatVersion: "0.6.1")
        RequestContext ctx = TestUtils.buildMockRequestContext(jsonFile, pharmML_061)
        Map<String, List<String>> indexData = ctx.partialData
        assertTrue(indexData.size() > 0)
        int old = indexData.size()
        ModelIndexer indexer = ModelIndexerFactory.createIndexer(ctx)
        def time = System.nanoTime()
        indexer.extractFileContent(ctx)
        time = (System.nanoTime() - time) / 1000000.0
        logger.info "It took ${time}ms to index the model."
        int newish = indexData.size()
        indexData.each { key, value ->
            logger.info("$key: $value")
        }
        logger.info("size is now $newish, not $old")
        assertTrue(newish > old)
    }

    @Test
    void testIndexingSBML() {
        File f = new File("src/test/resources/models/sbml-anno/indexData.json")
        def sbmlL2V4 = new ModelFormat(name: "SBML", identifier: "SBML", formatVersion: "L2V4")
        RequestContext ctx = TestUtils.buildMockRequestContext(f, sbmlL2V4)
        def indexer = ModelIndexerFactory.createIndexer(ctx)
        def time = System.nanoTime()
        indexer.extractFileContent(ctx)
        time = (System.nanoTime() - time) / 1000000.0
        logger.info "It took ${time}ms to index the SBML model."
        def idxData = ctx.partialData
        assertTrue(idxData.size() > 0)
        idxData.each { key, value ->
            logger.info("$key: $value")
        }
    }

    @Test
    void testIndexingUnknownFormat() {
        def f = new File("src/test/resources/models/annotatedUnknownFormat/indexData.json")
        def fmt = new  ModelFormat(name: "Unknown", identifier: "unknown", formatVersion: "*")
        RequestContext ctx = TestUtils.buildMockRequestContext(f, fmt)
        ModelIndexer indexer = ModelIndexerFactory.createIndexer(ctx)
        def data = ctx.partialData
        int size = data.size()
        assertTrue size > 0
        indexer.extractFileContent(ctx)
        int newSize = data.size()
        logger.info("size is now $newSize, not $size")
        assertTrue newSize > size
    }
}

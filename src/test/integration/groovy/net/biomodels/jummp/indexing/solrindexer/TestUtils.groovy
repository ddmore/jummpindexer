package net.biomodels.jummp.indexing.solrindexer

import net.biomodels.jummp.model.Model
import net.biomodels.jummp.model.ModelFormat
import net.biomodels.jummp.model.Revision
import net.biomodels.jummp.plugins.security.Person
import net.biomodels.jummp.plugins.security.User

import static org.junit.Assert.*

/**
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 */
final class TestUtils {
    /**
     * Convenience method for bootstrapping an integration test.
     *
     * Uses the supplied JSON file to construct a RequestContext. The context's configFilePath
     * will be set to the the actual path to .jummp.properties, which is fetched in the
     * usual way. A model and a revision (having the supplied @p format) are persisted to the
     * database to ensure that the request context being built references the correct model and
     * revision identifiers.
     *
     * @param jsonFile the indexData.json from which to extract contextual information.
     * @param format the format of the model associated with this requestContext
     * @return a fully-populated RequestContext.
     */
    static RequestContext buildMockRequestContext(File jsonFile, ModelFormat format) {
        RequestContext ctx = new RequestContext(jsonFile.absolutePath)
        assertFalse ctx.isEmpty()
        String jummpPropertiesPath = System.getenv("JUMMP_CONFIG")
        if (!jummpPropertiesPath) {
            String home = System.getProperty('user.home')
            jummpPropertiesPath = "$home${File.separator}.jummp.properties"
        }
        ctx.configFilePath = jummpPropertiesPath
        GormUtil.isTestEnvironment = true
        GormUtil.initGorm(ctx)

        Person p = new Person(userRealName: 'not me', institution: "M.E. PLC")
        assertNotNull(p.save())
        assertFalse(p.hasErrors())
        User self = new User(username: 'me', email: 'my@self.name', person: p,
            passwordExpired: false, password: 'obscure', accountExpired: false,
            accountLocked: false, enabled: true)
        assertNotNull(self.save())
        assertFalse(self.hasErrors())
        Model model = new Model(vcsIdentifier: "test/", submissionId: "MODEL00000400")
        assertNotNull(format.save())
        assertFalse(format.hasErrors())
        def revision = new Revision(model: model, vcsId: "1", revisionNumber: 1,
            owner: self, minorRevision: false, name:"interesting model v1",
            uploadDate: new Date(), format: format)
        assertTrue(revision.validate())
        /*
         * Normally, we would now call
         *     model.addToRevisions(revision)
         * but until we upgrade to GORM v4, there is a problem that precludes
         * dynamic methods from being added to the metaClass of domain objects
         * in applications that only use GORM, rather than the full Grails stack.
         *
         * One workaround is to employ DefaultGroovyMethods.setProperty(), which is
         * what we've done here, another would be to use the meta class of
         * HibernateUtils.groovy to call gormEnhancer.enhance(entity, false), just like
         * the patch below does.
         *
         * @see https://github.com/grails/grails-data-mapping/commit/ddf8e181
         */
        model.revisions = [revision]
        assertNotNull(model.save(flush: true))
        assertFalse(model.hasErrors())
        assertNotNull(revision.save(flush: true))
        assertFalse(revision.hasErrors())
        ctx.partialData['model_id'] = revision.model.id
        ctx.partialData['revision_id'] = revision.id
        AnnotationReferenceResolver.initialiseWithProperties(jummpPropertiesPath)
        ctx
    }
}

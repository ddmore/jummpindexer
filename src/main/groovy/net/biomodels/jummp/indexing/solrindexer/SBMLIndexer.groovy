/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import net.biomodels.jummp.annotationstore.ResourceReference
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.sbml.jsbml.*
import org.sbml.jsbml.ext.comp.CompModelPlugin
import org.sbml.jsbml.ext.comp.CompSBMLDocumentPlugin
import org.sbml.jsbml.ext.fbc.FBCModelPlugin
import org.sbml.jsbml.ext.fbc.FBCSpeciesPlugin
import org.sbml.jsbml.ext.render.RenderLayoutPlugin
import org.sbml.jsbml.ext.render.RenderListOfLayoutsPlugin
import org.sbml.jsbml.ext.spatial.SpatialCompartmentPlugin
import org.sbml.jsbml.ext.spatial.SpatialModelPlugin
import org.sbml.jsbml.ext.spatial.SpatialParameterPlugin

import javax.swing.tree.TreeNode
import net.biomodels.jummp.annotationstore.Qualifier
/**
 *
 * @author raza
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 */
class SBMLIndexer extends AbstractModelIndexer {
    private static final Logger logger = LoggerFactory.getLogger(SBMLIndexer.class)
    public static final boolean IS_INFO_ENABLED = logger.isInfoEnabled()
    private static final boolean IS_DEBUG_ENABLED = logger.isDebugEnabled()
    private Set<SBase> indexed = new HashSet<>()
    private RequestContext ctx
    private final def qualifierAccesion = ["BQB_ENCODES":"encodes",
                                     "BQB_HAS_PART": "hasPart",
                                     "BQB_HAS_PROPERTY": "hasProperty",
                                     "BQB_HAS_TAXON":"hasTaxon",
                                     "BQB_HAS_VERSION": "hasVersion",
                                     "BQB_IS": "is",
                                     "BQB_IS_DESCRIBED_BY": "isDescribedBy",
                                     "BQB_IS_ENCODED_BY": "isEncodedBy",
                                     "BQB_IS_HOMOLOG_TO": "isHomologTo",
                                     "BQB_IS_PART_OF":"isPartOf",
                                     "BQB_IS_PROPERTY_OF": "isPropertyOf",
                                     "BQB_IS_VERSION_OF":"isVersionOf",
                                     "BQB_OCCURS_IN":"occursIn",
                                     "BQB_UNKNOWN": "unknown",
                                     "BQM_HAS_INSTANCE": "hasInstance",
                                     "BQM_IS": "is",
                                     "BQM_IS_DERIVED_FROM": "isDerivedFrom",
                                     "BQM_IS_DESCRIBED_BY": "isDescribedBy",
                                     "BQM_IS_INSTANCE_OF":"isInstanceOf",
                                     "BQM_UNKNOWN":"unknown"
                                    ]

    public void extractFileContent(RequestContext ctx) {
        this.ctx = ctx
        def indexData = ctx.partialData
        def modelFilePath = ctx.mainFilePaths.first()
        final String SID = indexData.submissionId
        if (IS_INFO_ENABLED) {
            logger.info "Started indexing content from $SID ($modelFilePath)"
        }
        File file=new File(modelFilePath)
        if (IS_DEBUG_ENABLED) {
            logger.debug "Starting JSBML parsing for $SID"
        }
        SBMLDocument document=(new SBMLReader()).readSBMLFromStream(
                new ByteArrayInputStream(file.getBytes()))
        if (IS_DEBUG_ENABLED) {
            logger.debug "Finished JSBML parsing for $SID"
        }
        def model = document.getModel()
        indexData.put("elementName", [] as HashSet)
        indexData.put("elementDescription", [] as HashSet)
        indexData.put("elementID", [] as HashSet)
        indexData.put("externalReferenceID", [] as HashSet)
        indexData.put("externalReferenceName", [] as HashSet)
        indexData.put("modelAuthors", [] as HashSet)
        indexData.put("vCardModelAuthors", [] as HashSet)
        indexModel(indexData, model)
        if (IS_INFO_ENABLED) {
            logger.info "Finished indexing content from $SID"
        }
    }

    private void indexModel(Map indexData, Model model) {
        final String SID = indexData.submissionId
        if (IS_INFO_ENABLED) {
            logger.info "Started indexing model $SID."
        }
        if (IS_DEBUG_ENABLED) {
            logger.debug("Index packages for $SID")
        }
        indexPackages(indexData, model)
        if (IS_DEBUG_ENABLED) {
            logger.debug("Finished indexing packages for $SID")
        }
        // functions definition
        List functionDefs = getFunctionDefinitions(model)
        if (IS_DEBUG_ENABLED) {
            logger.debug("Indexing ${functionDefs.size()} function definitions for $SID")
        }
        functionDefs.each { funcDef ->
            populateMap(indexData, funcDef)
        }
        //species
        List allSpecies = getAllSpecies(model)
        if (IS_DEBUG_ENABLED) {
            logger.debug("Indexing ${allSpecies.size()} species for $SID")
        }
        allSpecies.each { species ->
            populateMap(indexData, species)
        }
        //compartments
        List compartments = getCompartments(model)
        if (IS_DEBUG_ENABLED) {
            logger.debug("Indexing ${compartments.size()} compartments for $SID")
        }
        compartments.each { compartment ->
            populateMap(indexData, compartment)
        }
        //parameters
        List parameters = getParameters(model)
            if (IS_DEBUG_ENABLED) {
            logger.debug("Indexing ${parameters.size()} parameters for $SID")
        }
        parameters.each { parameter ->
            populateMap(indexData, parameter)
        }
        //events
        List events = getEvents(model)
        if (IS_DEBUG_ENABLED) {
            logger.debug("Indexing ${events.size()} events for $SID")
        }
        events.each { event ->
            populateMap(indexData, event)
            event.assignments.each { assignment ->
                populateMap(indexData, assignment)
            }
        }
        //rules
        List rules = getRules(model)
        if (IS_DEBUG_ENABLED) {
            logger.debug("Indexing ${rules.size()} rules for $SID")
        }
        rules.each { rule ->
            populateMap(indexData, rule)
        }
        //reactions
        List reactions = getReactions(model)
        if (IS_DEBUG_ENABLED) {
            logger.debug("Indexing ${reactions.size()} reactions for $SID")
        }
        reactions.each { reaction ->
            populateMap(indexData, reaction)
            reaction.reactants.each { reactant ->
                populateMap(indexData, reactant)
            }
            reaction.products.each { product ->
                populateMap(indexData, product)
            }
            reaction.modifiers.each { modifier ->
                populateMap(indexData, modifier)
            }
        }
        // model annotations
        List annotations = getAnnotations(model)
        if (IS_DEBUG_ENABLED) {
            logger.debug("Indexing ${annotations.size()} annotations for $SID")
        }
        annotations.each { modelAnnos ->
            populateMap(indexData, modelAnnos)
        }
        // units
        List units = getUnits(model)
        if (IS_DEBUG_ENABLED) {
            logger.debug("Indexing ${units.size()} annotations for $SID")
        }
        units.each { unit ->
            populateMap(indexData, unit)
        }
        // initial assignments
        List assignments = getInitialAssignments(model)
        if (IS_DEBUG_ENABLED) {
            logger.debug("Indexing ${assignments.size()} initial assignments for $SID")
        }
        assignments.each { initialAssignment ->
            populateMap(indexData, initialAssignment)
        }
        // constraints
        List constraints = getConstraints(model)
        if (IS_DEBUG_ENABLED) {
            logger.debug("Indexing ${constraints.size()} constraints for $SID")
        }
        constraints.each { constraint ->
            populateMap(indexData, constraint)
        }
        // add model authors
        if (IS_DEBUG_ENABLED) {
            logger.debug("Indexing model authors for $SID")
        }
        model.getHistory().getListOfCreators().each { creator ->
            StringBuilder vCardData=new StringBuilder();
            if (creator.isSetGivenName()) {
                vCardData.append(creator.getGivenName());
                vCardData.append(" ");
            }
            if (creator.isSetFamilyName()) {
                vCardData.append(creator.getFamilyName())
            }
            addIfExists(indexData, "modelAuthors", vCardData.toString());
            if (creator.isSetEmail()) {
                vCardData.append(" ")
                vCardData.append(creator.getEmail())
            }
            if (creator.isSetOrganisation()) {
                vCardData.append(" ")
                vCardData.append(creator.getOrganisation())
            }
            addIfExists(indexData, "vCardModelAuthors", vCardData.toString())
        }

        //second pass -> tree traversal based indexing
        if (IS_DEBUG_ENABLED) {
            logger.debug("First pass of $SID done. ${indexData.toString().length()} chars")
        }
        indexTreeNode(indexData, model)
        if (IS_DEBUG_ENABLED) {
            logger.debug("Second pass of $SID done. ${indexData.toString().length()} chars")
        }
        if (IS_INFO_ENABLED) {
            logger.info "Finished indexing model $SID."
        }
    }

    private void indexTreeNode(Map indexData, def node) {
        if (node instanceof SBase) {
            addSBaseToIndex(indexData, node)
        }
        if (node instanceof TreeNode) {
            node.children().each {
                indexTreeNode(indexData, it)
            }
        }
    }

    private void handleQual(Map indexData, def element) {
        if (element.isSetPlugin("qual")) {
            def plugin = element.getPlugin("qual")
            plugin.getListOfQualitativeSpecies().each {
                addSBaseToIndex(indexData, it)
            }
            plugin.getListOfTransitions().each {
                addSBaseToIndex(indexData, it)
                it.getListOfFunctionTerms().each() { funcs ->
                    addSBaseToIndex(indexData, funcs)
                }
                it.getListOfOutputs().each() { output ->
                    addSBaseToIndex(indexData, output)
                    addSBaseToIndex(indexData, output.getDerivedUnitDefinition())
                    addIfExists(indexData, "elementDescription", output.getQualitativeSpecies())
                }
                it.getListOfInputs().each() { input ->
                    addSBaseToIndex(indexData, input)
                    addSBaseToIndex(indexData, input.getDerivedUnitDefinition())
                    addIfExists(indexData, "elementDescription", input.getQualitativeSpecies())
                }
            }
        }
    }

    private void handleArrays(Map indexData, def element) {
        if (element.isSetPlugin("arrays")) {
            def plugin = element.getPlugin("arrays")
            plugin.getListOfDimensions().each {
                addSBaseToIndex(indexData, it)
            }
        }
    }

    private void handleReq(Map indexData, def element) {
        if (element.isSetPlugin("req")) {
            def plugin = element.getPlugin("req")
            plugin.getListOfChangedMaths().each {
                addSBaseToIndex(indexData, it)
                addIfExists(indexData, "elementDescription", it.getChangedBy())
            }
        }
    }

    private void handleSpatial(Map indexData, def element) {
        if (element.isSetPlugin("spatial")) {
            def plugin = element.getPlugin("spatial")
            if (plugin instanceof SpatialCompartmentPlugin) {
                def compartmentMapping = plugin.getCompartmentMapping()
                addSBaseToIndex(indexData, compartmentMapping)
                addSBaseToIndex(indexData, compartmentMapping.getCompartmentInstance())
                addSBaseToIndex(indexData, compartmentMapping.getDomainTypeInstance())
            }
            if (plugin instanceof SpatialParameterPlugin) {
                try {
                    def param = plugin.getParamType()
                    addSBaseToIndex(indexData, param)
                    indexGeometry(indexData, param.getGeometryInstance())
                    addSBaseToIndex(indexData, param.getSpeciesReference())
                    addSBaseToIndex(indexData, param.getVariableInstance())
                    addIfExists(indexData, "elementDescription", param.getSpId())
                    addIfExists(indexData, "elementDescription", param.getVariable())
                }
                catch(Exception e) {
                    e.printStackTrace()
                }
            }
            if (plugin instanceof SpatialModelPlugin) {
                def geometry = plugin.getGeometry()
                indexGeometry(indexData, geometry)
            }
        }
    }

    private void indexGeometry(Map indexData, def geometry) {
        try {
            if (geometry) {
                addSBaseToIndex(indexData, geometry)
                geometry.getListOfAdjacentDomains().each() { doms ->
                    addSBaseToIndex(indexData, doms)
                    addIfExists(indexData, "elementDescription", doms.getDomain1())
                    addIfExists(indexData, "elementDescription", doms.getDomain2())
                }
                geometry.getListOfCoordinateComponents().each() { lcc ->
                    addSBaseToIndex(indexData, lcc)
                    addSBaseToIndex(indexData, lcc.getBoundaryMaximum())
                    addSBaseToIndex(indexData, lcc.getBoundaryMinimum())
                    addSBaseToIndex(indexData, lcc.getDerivedUnitDefinition())
                }
                geometry.getListOfDomains().each() { doms ->
                    addSBaseToIndex(indexData, doms)
                    doms.getListOfInteriorPoints().each() { ip ->
                        addSBaseToIndex(indexData, ip)
                    }
                }
                geometry.getListOfDomainTypes().each() { domType ->
                    addSBaseToIndex(indexData, domType)
                }
                geometry.getListOfGeometryDefinitions().each() { geoDef ->
                    addSBaseToIndex(indexData, geoDef)
                }
                geometry.getListOfSampledFields().each() { field ->
                    addSBaseToIndex(indexData, field)
                    addIfExists(indexData, "elementDescription", field.getDataString())
                }
            }
        }
        catch(Exception e) {
            e.printStackrace()
        }
    }

    private void handleMulti(Map indexData, def element) {
        if (element.isSetPlugin("multi")) {
            def plugin = element.getPlugin("multi")
            plugin.getListOfSelectors().each { sel
                addSBaseToIndex(indexData, sel)
                sel.getListOfBonds().each() { bond ->
                    addSBaseToIndex(indexData, bond)
                }
                sel.getListOfSpeciesTypeStates().each() { sts ->
                    addSBaseToIndex(indexData, sts)
                    sts.getListOfContainedSpeciesTypes().each() { stsCST ->
                        addSBaseToIndex(indexData, stsCST)
                        addIfExists(indexData, "elementDescription",
                            stsCST.getSpeciesTypeState())
                    }
                    sts.getListOfStateFeatureInstances().each() { stsSFI ->
                        addSBaseToIndex(indexData, stsSFI)
                        stsSFI.getListOfStateFeatureValues().each() { stsSFIsfv ->
                            addSBaseToIndex(indexData, stsSFIsfv)
                            addIfExists(indexData, "elementDescription",
                                stsSFIsfv.getPossibleValue())
                        }
                    }
                    addIfExists(indexData, "elementDescription", sts.getSpeciesType())
                }
                sel.getListOfUnboundBindingSites().each() { ubs ->
                    addSBaseToIndex(indexData, ubs)
                }
            }
            plugin.getListOfSpeciesTypes().each() { species ->
                addSBaseToIndex(indexData, species)
                species.getListOfStateFeatures().each() { stateFeat ->
                    addSBaseToIndex(indexData, stateFeat)
                    stateFeat.getListOfPossibleValues().each() { pv ->
                        addSBaseToIndex(indexData, pv)
                    }
                }
            }
        }
    }

    private void handleLayout(Map indexData, def element) {
        if (element.isSetPlugin("layout") || element.isSetPlugin("layout_L2")) {
            def plugin = element.getPlugin("layout")
            if (!plugin) {
                plugin = element.getPlugin("layout_L2")
            }
            plugin.getListOfLayouts().each { layout ->
                addSBaseToIndex(indexData, layout)
                addSBaseToIndex(indexData, layout.getDimensions())
                layout.getListOfCompartmentGlyphs().each() { comGly ->
                    addSBaseToIndex(indexData, comGly)
                    addSBaseToIndex(indexData, comGly.getCompartmentInstance())
                }
                layout.getListOfAdditionalGraphicalObjects().each() { go ->
                    addSBaseToIndex(indexData, go)
                    addSBaseToIndex(indexData, go.getBoundingBox())
                    addSBaseToIndex(indexData, go.getBoundingBox().getDimensions())
                    addSBaseToIndex(indexData, go.getBoundingBox().getPosition())
                }
                layout.getListOfReactionGlyphs().each() { reactGly ->
                    addSBaseToIndex(indexData, reactGly)
                    reactGly.getListOfSpeciesReferenceGlyphs().each() { srg ->
                        addSBaseToIndex(indexData, srg)
                        addSBaseToIndex(indexData, srg.getCurve())
                        addSBaseToIndex(indexData, srg.getSpeciesGlyphInstance())
                        addIfExists(indexData, "elementDescription", srg.getSpeciesReference())
                    }
                    addIfExists(indexData, "elementDescription", reactGly.getReaction())
                }
                layout.getListOfTextGlyphs().each()  { textGly ->
                    addSBaseToIndex(indexData, textGly)
                    addSBaseToIndex(indexData, textGly.getOriginOfTextInstance())
                    addIfExists(indexData, "elementDescription", textGly.getText())
                }
            }
        }
    }

    private void handleComp(Map indexData, def element) {
        if (element.isSetPlugin("comp")) {
            def plugin = element.getPlugin("comp")
            plugin.getListOfReplacedElements().each {
                addSBaseToIndex(indexData, it)
                addIfExists(indexData, "elementDescription", it.getDeletion())
            }
            addSBaseToIndex(indexData, plugin.getReplacedBy())
            if (plugin instanceof CompModelPlugin) {
                plugin.getListOfSubmodels().each {
                    addSBaseToIndex(indexData, it)
                    it.getListOfDeletions().each { deletion ->
                        addSBaseToIndex(indexData, deletion)
                    }
                }
                plugin.getListOfPorts().each {
                    addSBaseToIndex(indexData, it)
                }
            }
            else if (plugin instanceof CompSBMLDocumentPlugin) {
                plugin.getListOfExternalModelDefinitions().each {
                    addSBaseToIndex(indexData, it)
                    addIfExists(indexData, "elementDescription", it.getMd5())
                    addIfExists(indexData, "elementDescription", it.getSource())
                }
                plugin.getListOfModelDefinitions().each {
                    addSBaseToIndex(indexData, it)
                    addIfExists(indexData, "elementDescription", it.getMd5())
                    addIfExists(indexData, "elementDescription", it.getSource())
                    indexModel(indexData, it)
                }
            }
        }
    }

    private void handleGroups(Map indexData, def element) {
        if (element.isSetPlugin("groups")) {
            def plugin = element.getPlugin("groups")
            plugin.getListOfGroups().each { group ->
                addSBaseToIndex(indexData, group)
                group.getListOfMembers().each { member ->
                    addSBaseToIndex(indexData, member)
                    addIfExists(indexData, "elementID", member.getIdRef())
                }
                group.getListOfMemberConstraints().each { constraint ->
                    addSBaseToIndex(indexData, constraint)
                }
            }
        }
    }

    private void handleFbc(Map indexData, def element) {
        if (element.isSetPlugin("fbc")) {
            def plugin = element.getPlugin("fbc")
            if (plugin instanceof FBCModelPlugin) {
                plugin.getListOfFluxBounds().each {
                    addSBaseToIndex(indexData, it)
                }
            }
            else if (plugin instanceof FBCSpeciesPlugin) {
                plugin.getParent().each { species ->
                    addSBaseToIndex(indexData, species)
                }
                addIfExists(indexData, "elementDescription", plugin.getChemicalFormula())
            }
        }
    }

    private void handleRender(Map indexData, def element) {
        if (element.isSetPlugin("render")) {
            def plugin = element.getPlugin("render")
            indexGlobalRenderInfo(indexData, plugin.getRenderInformation())
            if (plugin instanceof RenderLayoutPlugin) {
                plugin.getListOfLocalRenderInformation().each {
                    addSBaseToIndex(indexData, it)
                    it.getListOfLocalStyles().each() { ls->
                        indexStyle(indexData, ls)
                    }
                }
            }
            else if (plugin instanceof RenderListOfLayoutsPlugin) {
                plugin.getListOfGlobalRenderInformation().each { gri ->
                    indexGlobalRenderInfo(indexData, gri)
                }
            }
        }
    }

    private void indexGlobalRenderInfo(Map indexData, def gri) {
        if (gri) {
            addSBaseToIndex(indexData, gri)
            gri.getListOfStyles().each() { styles ->
                indexStyle(indexData, style)
            }
        }
    }

    private String getNamespace(String qualifier) {
        String namespace = qualifier.split("_")[0];
        switch(namespace) {
            case "BQM": return "http://biomodels.net/model-qualifiers/"
            case "BQB": return "http://biomodels.net/biology-qualifiers/"
        }
        return "unknown"
    }

    private String getAccession(String qualifier) {
        String accession = qualifierAccesion[qualifier]
        if (!accession) {
            accession = "unknown"
        }
        return accession
    }

    private void indexAnnotations(Map indexData, def annotation, String subjectId) {
        annotation.each { anno ->
            String namespace = getNamespace(anno.qualifier)
            String accession = getAccession(anno.qualifier)
            Qualifier qual = Qualifier.findByAccessionAndNamespace(accession, namespace)
            if (qual==null) {
                qual = new Qualifier(accession:accession, namespace: namespace)
                if (accession.compareTo("unknown")!=0) {
                    qual.uri = namespace+accession
                }
                if (anno.biologicalQualifier) {
                    qual.qualifierType = "BiologicalQualifier"
                }
                else if (anno.modelQualifier) {
                    qual.qualifierType = "ModelQualifier"
                }
                else {
                    qual.qualifierType = "UnknownQualifier"
                }
                qual.save(failOnError: true)
            }
            anno.resources.each { resource ->
                addIfExists(indexData, "externalReferenceID", extractAccession(resource.urn))
                ResourceReference r = indexingStrategy.indexResourceReferenceAncestors(
                        resource.urn, ctx)
                indexingStrategy.saveAnnotation(subjectId, qual, r, ctx)
            }
        }
    }

    private void indexStyle(Map indexData, def styles) {
        addSBaseToIndex(indexData, styles)
        styles.getRoleList().each() { role ->
            addIfExists(indexData, "elementDescription", role)
        }
        styles.getTypeList().each() { type ->
            addIfExists(indexData, "elementDescription", type)
        }
        def group = styles.getGroup()
        addSBaseToIndex(indexData, group)
        group.getListOfElements().each() { el ->
            addSBaseToIndex(indexData, el)
        }
        addIfExists(indexData, "elementDescription", group.getEndHead())
        addIfExists(indexData, "elementDescription", group.getStartHead())
        addIfExists(indexData, "elementDescription", group.getTextAnchor())
        addIfExists(indexData, "elementDescription", group.getVTextAnchor())
    }

    private void indexPackages(Map indexData, def sbase) {
        handleQual(indexData, sbase)
        handleArrays(indexData, sbase)
        handleComp(indexData, sbase)
        handleFbc(indexData, sbase)
        handleGroups(indexData, sbase)
        handleLayout(indexData, sbase)
        handleMulti(indexData, sbase)
        handleRender(indexData, sbase)
        handleReq(indexData, sbase)
        handleSpatial(indexData, sbase)
    }

    private void populateMap(Map indexData, Map element) {
        addIfExists(indexData, "elementName", element.name)
        addIfExists(indexData, "elementID", element.id)
        addIfExists(indexData, "elementDescription", element.notes)
        if (element.sbase) {
            indexPackages(indexData, element.sbase)
        }
        if (element.annotation) {
            indexAnnotations(indexData, element.annotation, element.metaid)
        }
    }
    // TODO: very similar to the above, but called with a different data structure,
    // probably should be refactored
    private void addSBaseToIndex(Map indexData, def sbase) {
        if (sbase && !indexed.contains(sbase)) {
            indexed.add(sbase)
            if (sbase instanceof NamedSBase) {
                addIfExists(indexData, "elementID", sbase.id)
                addIfExists(indexData, "elementName", sbase.name)
                String sboTerm = sbase.getSBOTermID()
                if (sboTerm) {
                    addIfExists(indexData, "sbmlSBOTerm", sboTerm)
                }
            }
            addIfExists(indexData, "elementDescription", sbase.notesString)
            indexPackages(indexData, sbase)
            def annos = convertCVTerms(sbase.annotation)
            if (annos) {
                indexAnnotations(indexData, annos, sbase.getMetaId())
            }
        }
    }

    private String extractAccession(String urn) {
        if (urn.contains("http")) {
            String[] parts = urn.split("/");
            return parts[parts.length-1]
        }
        return urn
    }

    private void addIfExists(Map indexData, String key, String data) {
        if (data) {
            if (!indexData.containsKey(key)) {
                indexData.put(key, [])
            }
            indexData.get(key).add(data)
        }
    }

    public List<Map> getInitialAssignments(Model model) {
        List<Map> initialAssignments = []
        model.listOfInitialAssignments.each { initialAssignment ->
            initialAssignments << initialAssignmentToMap(initialAssignment)
        }
        return initialAssignments
    }

    private Map initialAssignmentToMap(InitialAssignment initialAssignment) {
        return [
                id: initialAssignment.metaId,
                name: initialAssignment.getVariable(),
                annotation: convertCVTerms(initialAssignment.annotation),
                sbase: initialAssignment
        ]
    }

    public List<Map> getConstraints(Model model) {
        List<Map> constraints = []
        model.listOfConstraints.each { constraint ->
            constraints << constraintToMap(constraint)
        }
        return constraints
    }

    private Map constraintToMap(Constraint constraint) {
        return [
                metaId: constraint.metaId,
                id: constraint.id,
                name: constraint.name,
                notes: constraint.notesString,
                annotation: convertCVTerms(constraint.annotation),
                sbase: constraint
        ]
    }

    public List<Map> getReactions(Model model) {
        List<Map> reactions = []
        model.listOfReactions.each { reaction ->
            reactions << reactionToMap(reaction)
        }
        return reactions
    }

    public List<Map> getUnits(Model model) {
        List<Map> units = [];
        model.getListOfUnitDefinitions().each { unit ->
            units << unitsToMap(unit)
        }
        return units
    }

    private Map unitsToMap(UnitDefinition unit) {
        return [
                metaId: unit.metaId,
                id: unit.id,
                name: unit.name,
                notes: unit.notesString,
                annotation: convertCVTerms(unit.annotation),
                sbase: unit
        ]
    }

    public List<Map> getRules(Model model) {
        List<Map> rules = []
        model.listOfRules.each { rule ->
            rules << ruleToMap(rule)
        }
        return rules
    }

    private Map ruleToMap(Rule rule) {
        String type = null
        Variable symbol = null
        if (rule instanceof RateRule) {
            type = "rate"
            symbol = rule.model.findSymbol(rule.variable)
        } else if (rule instanceof AssignmentRule) {
            type = "assignment"
            symbol = rule.model.findSymbol(rule.variable)
        } else if (rule instanceof AlgebraicRule) {
            type = "algebraic"
            symbol = rule.derivedVariable
        }

        return [
                //metaId: rule.metaId,
                //math: rule.getMathMLString(),
                //variableId : symbol ? symbol.id : null,
                //variableName: symbol ? symbol.name : null,
                //variableType: symbol ? symbol.elementName : null,
                //type: type,
                notes: rule.notesString,
                name: symbol ? symbol.name : null,
                annotation: convertCVTerms(rule.annotation),
                sbase: rule
        ]
    }

    private List<Map> convertSpeciesReferences(List<SimpleSpeciesReference> list) {
        List<Map> species = []
        list.each {
            if (it instanceof SpeciesReference) {
                species << speciesReferenceToMap(it)
            } else {
                species << [species: it.species, speciesName: it.model.getSpecies(it.species).name]
            }
        }
        return species
    }

    private Map speciesReferenceToMap(SpeciesReference reference) {
        return [
                id: reference.species,
                name: reference.model.getSpecies(reference.species)?.name,
                constant: reference.constant,
                stoichiometry: reference.stoichiometry,
                notes: reference.notesString,
                annotation: convertCVTerms(reference.annotation),
                sbase: reference
        ]
    }

    private Map reactionToMap(Reaction reaction) {

        return [
                id: reaction.id,
                metaId: reaction.metaId,
                name: reaction.name,
                reversible: reaction.reversible,
                sbo: sboName(reaction),
                reactants: convertSpeciesReferences(reaction.listOfReactants),
                products: convertSpeciesReferences(reaction.listOfProducts),
                modifiers: convertSpeciesReferences(reaction.listOfModifiers),
                notes: reaction.notesString,
                annotation: convertCVTerms(reaction.annotation),
                sbase: reaction
        ]
    }

    public List<Map> getEvents(Model model) {
        List<Map> events = []
        model.listOfEvents.each { event ->
            events << eventToMap(event)
        }
        return events
    }

    private Map eventToMap(Event event) {
        return [
                id: event.id,
                metaId: event.metaId,
                name: event.name,
                assignments: eventAssignmentsToList(event.listOfEventAssignments),
                notes: event.notesString,
                annotation: convertCVTerms(event.annotation),
                sbase: event
        ]
    }

    private List<Map> eventAssignmentsToList(List<EventAssignment> assignments) {
        List<Map> eventAssignments = []
        assignments.each { assignment ->
            Symbol symbol = assignment.model.findSymbol(assignment.variable)
            eventAssignments << [
                    metaId: assignment.metaId,
                    math: assignment.mathMLString,
                    variableId: assignment.variable,
                    variableName: symbol ? symbol.name : "",
                    variableType: symbol ? symbol.elementName : "",
                    notes: assignment.notesString,
                    annotation: convertCVTerms(assignment.annotation),
                    sbase: assignment
            ]
        }
        return eventAssignments
    }

    private List<Map> getAllSpecies(Model model) {
        List<Map> allSpecies = []
        model.listOfSpecies.each { species ->
            allSpecies << speciesToMap(species)
        }
        return allSpecies
    }

    public List<Map> getCompartments(Model model) {
        List<Map> compartments = []
        model.listOfCompartments.each { compartment ->
             compartments << compartmentToMap(compartment)
        }
        return compartments
    }

    private Map compartmentToMap(Compartment compartment) {
        return [
                metaId: compartment.metaId,
                id: compartment.id,
                name: compartment.name,
                size: compartment.size,
                spatialDimensions: compartment.getSpatialDimensions(),
                units: compartment.units,
                sbo: sboName(compartment),
                notes: compartment.notesString,
                annotation: convertCVTerms(compartment.annotation),
                sbase: compartment
        ]
    }

    private Map parameterToMap(QuantityWithUnit parameter) {
        return [
                id: parameter.id,
                name: parameter.name,
                metaId: parameter.metaId,
                constant: (parameter instanceof Parameter) ? parameter.constant : true,
                value: parameter.isSetValue() ? parameter.value : null,
                sbo: sboName(parameter),
                unit: parameter.units,
                notes: parameter.notesString,
                annotation: convertCVTerms(parameter.annotation),
                sbase: parameter
        ]
    }

    public List<Map> getParameters(Model model) {
        ListOf<Parameter> parameters = model.getListOfParameters()
        List<Map> list = []
        parameters.each { parameter ->
            list << parameterToMap(parameter)
        }
        return list
    }

    private Map speciesToMap(Species species) {
        def initialAmount
        def initialConcentration
        if (species.isSetInitialAmount()) {
            initialAmount = species.initialAmount
        } else {
            initialAmount = null
        }
        if (species.isSetInitialConcentration()) {
            initialConcentration = species.initialConcentration
        } else {
            initialConcentration = null
        }
        return [
                metaid: species.metaId,
                id: species.id,
                name: species.name,
                compartment: species.compartment,
                initialAmount: initialAmount,
                initialConcentration: initialConcentration,
                substanceUnits: species.substanceUnits,
                notes: species.notesString,
                annotation: convertCVTerms(species.annotation),
                sbo: sboName(species),
                sbase: species
        ]
    }

    private List<Map> getFunctionDefinitions(Model model) {
        List<Map> functions = []
        model.listOfFunctionDefinitions.each { function ->
            functions << functionDefinitionToMap(function)
        }
        return functions
    }

    private Map functionDefinitionToMap(FunctionDefinition function) {
        return [
                id: function.id,
                name: function.name,
                notes: function.notesString,
                sbo: sboName(function),
                annotation: convertCVTerms(function.annotation),
                sbase: function
        ]
    }


    public List<Map> getAnnotations(Model model) {
        return [ [annotation: convertCVTerms(model.annotation)] ]
    }

    private List<Map> convertCVTerms(Annotation annotation) {
        List<Map> list = []
        if (annotation) {
            annotation.listOfCVTerms.each { cvTerm ->
                list << [
                        qualifier: cvTerm.biologicalQualifier ?
                            cvTerm.biologicalQualifierType.toString() :
                            (cvTerm.modelQualifier ?
                                cvTerm.modelQualifierType.toString() : ""),
                        biologicalQualifier: cvTerm.biologicalQualifier,
                        modelQualifier: cvTerm.modelQualifier,
                        resources: cvTerm.resources.collect {
                            Map data = [:]
                            data.put("urn", it)
                            data
                        }
                ]
            }
        }
        return list
    }

    private Map sboName(SBase sbase) {
        try {
            String name = SBO.getTerm(sbase.getSBOTerm()).name
            Map map = [:];
            String sboTermId = java.net.URLEncoder.encode(sbase.getSBOTermID(), "UTF-8")
            map.put("urn", "urn:miriam:obo.sbo:$sboTermId")
            map.put("name", name)
            return map
        } catch (NoSuchElementException e) {
            return [:]
        }
    }
}

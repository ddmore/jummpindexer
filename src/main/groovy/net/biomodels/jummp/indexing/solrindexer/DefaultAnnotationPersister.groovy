/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import groovy.transform.CompileStatic
import net.biomodels.jummp.annotationstore.ElementAnnotation
import net.biomodels.jummp.annotationstore.Statement
import net.biomodels.jummp.model.Revision
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Simple concrete strategy for saving annotations to the database.
 *
 * @author Raza Ali
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 */
@CompileStatic
class DefaultAnnotationPersister implements AnnotationPersister {
    /**
     * The class logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DefaultAnnotationPersister.class)

    /**
     * Saves a given Statement to the database.
     *
     * @see net.biomodels.jummp.annotationstore.Statement
     * @param statement the statement to persist.
     * @param ctx the request's context.
     * @return the ElementAnnotation object corresponding to @p statement.
     */
    @Override
    ElementAnnotation save(Statement statement, RequestContext ctx) {
        Map<String, Object> indexData = ctx.partialData
        String creator = indexData.submitter
        long revisionId = (long) indexData.revision_id
        ElementAnnotation annotation = new ElementAnnotation(statement: statement,
            creatorId: creator, revision: Revision.load(revisionId))
        statement.annotation = annotation
        if (!statement.validate()) {
            final String uid = ctx.partialData.uniqueId
            log.error """\
Cannot persist statements for revision ${uid} - ${statement.dump()} ${statement.errors.allErrors}"""
            return null
        }
        if (!annotation.save()) {
            log.error "Cannot save annotation ${annotation.dump()}: ${annotation.errors.allErrors.dump()}"
        }
        return annotation
    }
}

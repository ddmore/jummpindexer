/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import com.hp.hpl.jena.rdf.model.Model
import eu.ddmore.metadata.service.MetadataValidator
import eu.ddmore.metadata.service.MetadataValidatorImpl
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationContext
import org.springframework.context.support.ClassPathXmlApplicationContext

import javax.xml.stream.XMLStreamReader

/**
 * Utility methods for extracting information from PharmML models.
 *
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 * @author Sarala Wimalaratne
 */
class PharmMLIndexerUtils {
    /**
     * the class logger
     */
    private static final Logger log = LoggerFactory.getLogger(PharmMLIndexerUtils.class)
    /**
     * logger verbosity threshold
     */
    private static final boolean IS_DEBUG_ENABLED = log.isDebugEnabled()

    /**
     * Traverses the supplied @p pharmML and extracts all element descriptions.
     *
     * @param pharmML the PharmML model to process.
     * @return a list of all descriptions contained within the model.
     */
    static List<String> extractElementDescriptions(File pharmML) {
        if (IS_DEBUG_ENABLED) {
            log.debug "Extracting element descriptions from ${pharmML.path}..."
        }
        final List<String> result = []
        if (pharmML && pharmML.canRead()) {
            JummpXmlUtils.parseXmlFile.curry(pharmML) { XMLStreamReader r ->
                if ("Description".equalsIgnoreCase(r.getLocalName())) {
                    result.add r.getElementText()
                }
                return false
            }
        } else {
            log.warn "Cannot extract element descriptions from file $pharmML"
        }
        if (IS_DEBUG_ENABLED) {
            log.debug("...finished extracting element annotations for ${pharmML.path}")
        }
        return result
    }

    /**
     * Finds the RDF file describing the context of the given @p pharmML file.
     *
     * @param pharmML the PharmML model to process.
     * @param allFiles the list of all files making up the current submission.
     *
     * @return the RDF file referenced by the supplied PharmML model.
     */
    static File findAnnotationFile(File pharmML, List<File> allFiles) {
        String metadataFileName = JummpXmlUtils.findModelAttribute(pharmML,
                "PharmML", "metadataFile")
        File rdfFile
        if (!metadataFileName) {
            // fall back to peaking inside allFiles for an RDF file.
            rdfFile = allFiles.find { File f ->
                f.name.endsWith('.rdf')  && JummpXmlUtils.containsElement(f, "RDF")
            }
            if (rdfFile) {
                log.warn("""\
PharmML submission ${pharmML.absolutePath} contains RDF file ${rdfFile.name}, but the model \
is missing the metadataFile attribute. Will treat ${rdfFile} as the annotation file.""")
            } else { // no metadataFile attribute in model, no RDF file in allFiles
                return null
            }
        } else { // found a value for the model's metadataFile attribute
            // check that there is an RDF file in allFiles by that name
            rdfFile = allFiles.find { File f ->
                f.name.equals(metadataFileName) && JummpXmlUtils.containsElement(f, "RDF")
            }
            if (!rdfFile) {
                log.warn("""\
PharmML submission ${pharmML.absolutePath} specifies that annotations should be present \
in file $metadataFileName, but it is not part of the submitted files.""")
            }
        }

        rdfFile
    }

    /**
     * Builds an in-memory representation of the supplied RDF file.
     *
     * @param rdfFile the RDF file to parse.
     * @return the Apache Jena Model corresponding to @p rdfFile.
     */
     static Model readModelAnnotations(File rdfFile) {
         ApplicationContext context = new ClassPathXmlApplicationContext("metadatalib-spring-config.xml");
         MetadataValidator reader = context.getBean(MetadataValidatorImpl.class);
         reader.read(rdfFile)
     }
}

/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import net.biomodels.jummp.annotationstore.ResourceReference
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Simple database-backed resource reference ancestry indexer.
 *
 * This concrete implementation uses the settings of the supplied request context to look for
 * a suitable resource reference in the database. It does not connect to any other external
 * sources of information.
 *
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 */

class SimpleResourceReferenceAncestryIndexer implements ResourceReferenceAncestryIndexer {
    /**
     * The class logger.
     */
    private final Logger log = LoggerFactory.getLogger(this.getClass())
    /**
     * Logging thresholds
     */
    private final boolean IS_DEBUG_ENABLED = log.isDebugEnabled()
    private final boolean IS_INFO_ENABLED = log.isInfoEnabled()

    /**
     * Creates or retrieves a ResourceReference for the given cross reference.
     *
     * @param uriOrLabel the cross reference for which to look up a resource reference.
     * @param ctx the current request's context.
     * @return a ResourceReference suitable for @p uriOrLabel.
     */
    @Override
    ResourceReference indexAncestors(String uriOrLabel, RequestContext ctx, String
        fieldName, Map<String, Collection<String>> thisStatement) {
        if (IS_DEBUG_ENABLED) {
            log.debug("Indexing ancestors for $uriOrLabel")
        }
        // Can't use CompileStatic because of dynamic finders
        ResourceReference reference = ResourceReference.findByUri(uriOrLabel)
        if (reference) {
            if (IS_INFO_ENABLED) {
                log.info("""\
Found matching xref ${reference.id} for $uriOrLabel -- ${reference.parents.size()} parents""")
            }
            def queue = [reference]
            if (reference.parents) {
                queue.addAll(reference.parents)
            }
            queue.each { ResourceReference r ->
                final String accession = r.accession
                if (accession) {
                    if (ctx.partialData.containsKey("externalReferenceID")) {
                        ((Collection) ctx.partialData.externalReferenceID).add(accession)
                    } else {
                        ctx.partialData.externalReferenceID = [accession]
                    }
                }
                final String name = r.name
                if (name) {
                    if (thisStatement.containsKey(fieldName)) {
                        thisStatement.get(fieldName).add(name)
                    } else {
                        thisStatement.put(fieldName, [name])
                    }
                    if (ctx.partialData.containsKey("externalReferenceName")) {
                        ctx.partialData.externalReferenceName.add(name)
                    } else {
                        ctx.partialData.externalReferenceName = [name]
                    }
                }
            }
        } else {
            reference = new ResourceReference(datatype: "unknown")
            if (uriOrLabel.startsWith("http:")) {
                log.warn "Found no references for uri $uriOrLabel"
                reference.uri = uriOrLabel
            } else {
                if (IS_INFO_ENABLED) {
                    log.info("Creating a new reference with name $uriOrLabel")
                }
                reference.name = uriOrLabel
            }
            if (!reference.save()) {
                log.error("resource $uriOrLabel not saved: ${reference.errors.allErrors.dump()}")
            }
        }
        if (IS_DEBUG_ENABLED) {
            log.debug("Finished indexing ancestry for $uriOrLabel")
        }
        reference
    }
}

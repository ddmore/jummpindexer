package net.biomodels.jummp.indexing.solrindexer.importer

import groovy.json.JsonSlurper
import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.indexing.solrindexer.RequestContext
import net.biomodels.jummp.indexing.solrindexer.GormUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Simple class for importing information from the DDMoRe PKPD ontology.
 *
 * This class works off a CSV export of the terms in the ontology in the following format:
 *  <category>, <label>, <term accession>
 *
 * The ontology terms appear in annotations using the namespace
 *          "http://www.ddmore.org/ontologies/ontology/pkpd-ontology"
 *
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 */
class PkpdOntologyImporter {
    /**
     * the class logger
     */
    private static final Logger logger = LoggerFactory.getLogger(PkpdOntologyImporter.class)
    /**
     * threshold for logging verbosity
     */
    private static final boolean IS_INFO_ENABLED = logger.isInfoEnabled()
    /**
     * The default namespace for terms in this ontology.
     */
    private static final String NS = "http://www.ddmore.org/ontologies/ontology/pkpd-ontology"
    /**
     * The csv file containing the ontology dump.
     */
    final InputStream csvMappingStream

    /* hide the default constructor */
    private PkpdOntologyImporter() {}

    /**
     * constructs an ontology importer that will extract information from the supplied path.
     * @param path the path to the csv dump of the PKPD ontology.
     */
    PkpdOntologyImporter(String path) {
        csvMappingStream = getClass().getResourceAsStream(path)
        if (IS_INFO_ENABLED) {
            logger.info("Importing ontology terms from mapping file ${path}")
        }
    }

    /**
     * Traverses the mapping file persisting the corresponding ResourceReference elements.
     */
    void parseOntologyMapping() {
        csvMappingStream.eachLine("UTF-8") { l ->
            String[] tokens = l.split(',')
            if (tokens.length != 3) {
                logger.error("Unexpected record $l in PKPD Ontology export.")
                return
            }
            String termId = tokens[2]
            if (termId.startsWith("\"pkpd:")) {
                termId = stripQuotes(termId).replace(':', '_')
                String termLabel = stripQuotes(tokens[1])
                persistTerm(termId, termLabel)
            }
        }
    }

    /*
     * Helper method to strips the first and last character from a string.
     * Useful if the second and third column of the mapping file contain quotes.
     */
    private String stripQuotes(String s) {
        if (!s || s.length() <= 2) {
            return s
        }
        return s.substring(1, s.length() - 1)
    }

    /* saves the term to the database */
    private void persistTerm(String termId, String label) {
        if (IS_INFO_ENABLED) {
            logger.info("Creating resource reference for ($termId, $label)")
        }
        String termURI = new StringBuilder(NS.length() + termId.length() + 1).append(NS).
                append('#').append(termId).toString()
        ResourceReference ref = new ResourceReference(accession: termId, name: label,
                datatype: 'pkpd', uri: termURI)
        if (!ref.save()) {
            logger.error "Can't save ResourceReference ${ref.dump()}"
        }
    }

    static void main(String[] args) {
        RequestContext ctx = buildRequestContext(args)
        GormUtil.initGorm(ctx)
        logger.error "No database settings available - will not be able to import anything."
        final String resourcePath = "/fields-default-20150302-mg.csv"
        def importer = new PkpdOntologyImporter(resourcePath)
        importer.parseOntologyMapping()
    }

    private static boolean isValidFile(File file) {
        file.exists() && file.isFile() && file.canRead()
    }

    private static RequestContext buildRequestContext(String[] args) {
        if (!args || args.length < 1) {
            System.err.println('''Please create a file with the following structure:
{
    "url": "<the database url>",
    "username": "<your_db_username>",
    "password": "<your_db_password>"
}''')
            throw new IllegalArgumentException("Missing path to file with db settings")
        }
        File dbSettings = new File(args[0])
        if (!isValidFile(dbSettings)) {
            throw new IllegalArgumentException("Can't find db settings inside ${args[0]}")
        }
        JsonSlurper slurper = new JsonSlurper()
        def settings = slurper.parse(new BufferedReader(new FileReader(dbSettings)))
        if (!settings) {
            throw new IllegalArgumentException("""\
Expected database url, username and password inside ${args[0]}""")
        }
        RequestContext ctx = new RequestContext()
        ctx.databaseURL = settings.url
        ctx.databaseUsername = settings.username
        ctx.databasePassword = settings.password
        ctx
    }
}

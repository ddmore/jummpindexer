/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import groovy.transform.CompileStatic

/**
 * Simple class for creating ModelIndexer implementations.
 *
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 */
@CompileStatic
class ModelIndexerFactory {
    /**
     * Simple factory method to instantiate indexers for a given request.
     *
     * @param ctx the RequestContext based on which to create the appropriate model indexer.
     * @return a model indexer capable of handling the current request.
     */
    static ModelIndexer createIndexer(RequestContext ctx) {
        IndexingStrategy indexingStrategy = createIndexingStrategy(ctx)
        switch (ctx.modelFormat) {
            case "SBML":
                return new SBMLIndexer(indexingStrategy: indexingStrategy, ctx: ctx)
            case "PharmML":
                return new PharmMlIndexer(indexingStrategy: indexingStrategy)
            default:
                return new GenericModelIndexer(indexingStrategy: indexingStrategy)
        }
    }

    protected static IndexingStrategy createIndexingStrategy(RequestContext ctx) {
        def ancestryIndexer = new ResourceReferenceAncestryIndexingMediator()
        switch (ctx.metadataProcessor) {
            case IndexingEnvironment.DDMORE:
            default:
                def pm = new DefaultAnnotationPersister()
                return new DDMoReIndexingStrategy(persistenceManager: pm,
                        ancestryIndexer: ancestryIndexer)
        }
    }
}

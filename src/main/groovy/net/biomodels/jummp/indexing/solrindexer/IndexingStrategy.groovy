/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import net.biomodels.jummp.annotationstore.ElementAnnotation
import net.biomodels.jummp.annotationstore.Qualifier
import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.annotationstore.Statement
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Contract for how a model should be indexed.
 *
 * Implementations are able to decide how a submission should be handled based on the
 * request context settings.
 *
 * @see AbstractModelIndexer
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 */
abstract class IndexingStrategy {
    /**
     * The class logger.
     */
    private static final Logger log = LoggerFactory.getLogger(IndexingStrategy.class)
    /**
     * Concrete strategy for saving annotations to the database.
     */
    AnnotationPersister persistenceManager
    /**
     * Mediator which uses an appropriate handler to index resource references.
     */
    ResourceReferenceAncestryIndexingMediator ancestryIndexer

    /**
     * Contract for processing a submission.
     *
     * Implementations of this method will modify the requestContext object according to
     * their own rules for handling the given submission.
     * @param ctx The RequestContext of the submission that should be indexed.
     */
    abstract void doIndexSubmission(RequestContext ctx)

    /**
     * Persists the supplied RDF statement into the database.
     *
     * @param subject the model element targeted by this statement.
     * @param predicate the qualifier of this statement.
     * @param object the object of this statement.
     * @param ctx the context of the current request.
     * @return an ElementAnnotation that has been persisted in the database.
     */
    ElementAnnotation saveAnnotation(String subject, Qualifier predicate,
            ResourceReference object, RequestContext ctx) {
        Statement s = new Statement(subjectId: subject, qualifier: predicate,
            object: object)
        // cannot validate s here because it has no associated elementAnnotation
        persistenceManager.save(s, ctx)
    }

    /**
     * Retrieves or creates a ResourceReference for the supplied cross reference.
     *
     * @see ResourceReferenceAncestryIndexer#indexAncestors(java.lang.String, net.biomodels.jummp.indexing.solrindexer.RequestContext,
     *          java.lang.String, java.util.Map)
     * @param xref the cross reference for which to perform the lookup
     * @param ctx the request's context
     * @return the ResourceReference which matches @p xref.
     */
    ResourceReference indexResourceReferenceAncestors(String xref, RequestContext ctx,
            String fieldName, Map currentStatement) {
        ancestryIndexer.indexAncestors(xref, ctx, fieldName, currentStatement)
    }
}

/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer.miriam

import groovy.transform.CompileStatic
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import uk.ac.ebi.miriam.db.MiriamLocalProvider

/**
 * Basic facade for interacting with Miriam API.
 *
 * The API requires that the system property 'miriam.xml.export' points to the path
 * where the export of the Registry is located. A default value is used to initialise
 * this property when the class is loaded, which facilitates unit testing. In production,
 * the JSON index file sent by Jummp is expected to contain the location of a recent version
 * of the Registry's export, hence the RequestContext needs to ensure that the system property
 * is updated accordingly.
 * @see net.biomodels.jummp.indexing.solrindexer.RequestContext
 *
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 */
@CompileStatic
class MiriamRegistryService {
    static final Logger logger = LoggerFactory.getLogger(MiriamRegistryService.class)
    static final boolean  IS_DEBUG_ENABLED = logger.isDebugEnabled()
    static final boolean  IS_INFO_ENABLED = logger.isInfoEnabled()
    static final MiriamLocalProvider miriamLocalProvider = new MiriamLocalProvider()
    static final String DEFAULT_EXPORT_PATH = "src/test/resources/miriam.xml"
    static final String EXPORT_PROPERTY = "miriam.xml.export"

    static {
        updateMiriamExportPath(DEFAULT_EXPORT_PATH)
    }

    /**
     * Returns a non-deprecated version of an annotation URI.
     *
     * The supplied URI needs to be that of a term in a collection present in the
     * MIRIAM Registry, otherwise null is returned.
     */
    static String updateURI(final String uri) {
        if (!uri?.trim()) {
            return uri
        }
        if (IS_DEBUG_ENABLED) {
            logger.debug("Checking if $uri has been deprecated in the Registry")
        }
        String officialURN = miriamLocalProvider.getMiriamURI(uri)
        String officialURL = miriamLocalProvider.convertURN(officialURN)
        if (IS_DEBUG_ENABLED) {
            logger.debug("The official URL for $uri is $officialURL")
        }
        return officialURL
    }

    /**
     * Updates the path to the export of the MIRIAM Registry.
     */
    static void updateMiriamExportPath(final String path = DEFAULT_EXPORT_PATH) {
        String exportPath = System.getProperty(EXPORT_PROPERTY)
        if (!exportPath && IS_DEBUG_ENABLED) {
            logger.debug("MIRIAM Registry export is not defined.")
        }
        System.setProperty(EXPORT_PROPERTY, path)
        if (IS_INFO_ENABLED) {
            logger.info("Using Registry export located at $path")
        }
    }
}

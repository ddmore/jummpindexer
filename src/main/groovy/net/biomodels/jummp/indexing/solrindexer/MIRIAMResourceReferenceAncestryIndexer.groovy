/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.solrindexer

import groovy.transform.CompileStatic
import net.biomodels.jummp.annotationstore.ResourceReference
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Strategy for extracting information about terms coming from hierarchical constructs such
 *
 * @author Raza Ali
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 */
@CompileStatic
class MIRIAMResourceReferenceAncestryIndexer implements ResourceReferenceAncestryIndexer {
    /**
     * The class logger.
     */
    static Logger log = LoggerFactory.getLogger(MIRIAMResourceReferenceAncestryIndexer.class)
    /**
     * Logging verbosity threshold.
     */
    private static final boolean IS_DEBUG_ENABLED = log.isDebugEnabled()

    /**
     * Convenience method for retrieving the current value for indexing field @p key.
     *
     * If the supplied field is not in the index yet, it is created.
     * @param key the name of the field to retrieve from the index document.
     * @param indexData the index document for the current request.
     * @return the collection of values contained in @p indexData for field @p key
     */
    protected Collection getOrCreate(String key, Map<String, Collection<String>> indexData) {
        if (!indexData.containsKey(key)) {
            indexData.put(key, [])
        }
        return indexData.get(key)
    }

    /**
     * Retrieves external information about MIRIAM URNs and Identifiers.org URIs.
     *
     * @see AnnotationReferenceResolver
     * @param xref the urn or url of the cross reference that should be indexed.
     * @param ctx the current request context.
     * @return the saved ResourceReference corresponding to @p xref.
     */
    @Override
    ResourceReference indexAncestors(String xref, RequestContext ctx, String fieldName,
            Map currentStatement) {
        if (IS_DEBUG_ENABLED) {
            log.debug("Looking for ancestors for xref $xref")
        }
        Map<String, Collection<String>> indexData = ctx.partialData
        def annoType = AnnotationReferenceResolver.instance().getType(xref)
        Set<Long> added = new HashSet<>()
        ResourceReference reference
        if (annoType != AnnotationReferenceResolver.AnnoType.UNKNOWN) {
            String[] urnInfo = AnnotationReferenceResolver.instance().extractParts(xref, annoType)
            if (AnnotationReferenceResolver.instance().isSupported(urnInfo[0])) {
                reference = AnnotationReferenceResolver.instance().resolve(urnInfo[1],
                        urnInfo[0])
                Set<ResourceReference> unprocessed = new HashSet<>()
                unprocessed.add(reference)
                while (!unprocessed.isEmpty()) {
                    Set<ResourceReference> nextIteration = new HashSet<>()
                    unprocessed.each { ResourceReference r ->
                        getOrCreate("externalReferenceID", indexData).add(r.accession)
                        getOrCreate("externalReferenceName", indexData).add(r.name)
                        getOrCreate("externalReferenceName", indexData).addAll(r.synonyms)
                        getOrCreate(fieldName, currentStatement).add(r.accession)
                        getOrCreate(fieldName, currentStatement).add(r.name)
                        getOrCreate(fieldName, currentStatement).addAll(r.synonyms)
                        added.add(r.id)
                        r.getParents().each { ResourceReference parent ->
                            if (!added.contains(parent.id)) {
                                nextIteration.add(parent)
                            }
                        }
                    }
                    unprocessed.clear()
                    unprocessed.addAll(nextIteration)
                }
            } else {
                reference = new ResourceReference(uri: xref, datatype: urnInfo[0])
                if (!reference.save()) {
                    log.error("Could not save resource with uri ${xref}")
                }
            }
        } else {
            reference = new ResourceReference(uri: xref, datatype: 'unknown')
            if (!reference.save()) {
                log.error("Could not save resource with uri ${xref}")
            }
        }
        if (IS_DEBUG_ENABLED) {
            log.debug("Returning ResourceReference $reference for xref $xref")
        }
        reference
    }
}
